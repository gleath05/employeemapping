
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { EmpMappingModule } from './features/emp-mapping/emp-mapping.module';
import { UserAdminModule } from './features/user-admin/user-admin.module';
import { ToastrModule } from 'ngx-toastr';
import { RightMappingComponent } from './features/emp-mapping/components/right-mapping/right-mapping.component';
import { AddEmpComponent } from './features/user-admin/components/add-emp/add-emp.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule} from '@angular/fire/database'
import { ListEmpComponent } from './features/user-admin/components/list-emp/list-emp.component';

@NgModule({
  declarations: [
    AppComponent,
    RightMappingComponent,
    AddEmpComponent,
    ListEmpComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    EmpMappingModule,
    UserAdminModule,
    ToastrModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
    