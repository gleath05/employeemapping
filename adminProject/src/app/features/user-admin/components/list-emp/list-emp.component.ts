import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EmployeeService } from 'src/app/features/shared/services/employee.service';

@Component({
  selector: 'app-list-emp',
  templateUrl: './list-emp.component.html',
  styleUrls: ['./list-emp.component.scss']
})
export class ListEmpComponent implements OnInit {

  formTemplate: FormGroup;
  empArray: any[];

  constructor
    (private service: EmployeeService,
      private fb: FormBuilder) { }

  ngOnInit() {
    this.formTemplate = this.fb.group({
      $key: [null],
      empId: [null, Validators.required],
      empFn: [null, Validators.required],
      empMn: [null, Validators.required],
      empLn: [null, Validators.required],
      empBday: [null, Validators.required],
      empPosition: [null, Validators.required],
      empDepartment: [null, Validators.required],
      imageUrl: [null, Validators.required],
    });

    this.service.getEmployeeDetails().subscribe(
      list => {
        this.empArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

}
