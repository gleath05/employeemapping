import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { EmployeeService } from 'src/app/features/shared/services/employee.service';

declare var require: any;
const shortId = require('shortid');

@Component({
  selector: 'app-add-emp',
  templateUrl: './add-emp.component.html',
  styleUrls: ['./add-emp.component.scss']
})
export class AddEmpComponent implements OnInit {
  showFiller = false;

  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;
  formTemplate: FormGroup;



  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private storage: AngularFireStorage,
    private service: EmployeeService,
  ) { }

  ngOnInit() {
    this.formTemplate = this.fb.group({
      empId: [null, Validators.required],
      empFn: [null, Validators.required],
      empMn: [null, Validators.required],
      empLn: [null, Validators.required],
      empBday: [null, Validators.required],
      empPosition: [null, Validators.required],
      empDepartment: [null, Validators.required],
      imageUrl: [null, Validators.required],
      selectImage: ['']
    });
    this.resetForm();
  }

  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.formTemplate.get('selectImage').patchValue(event.target.files[0]);
    } else {
      this.imgSrc = '/src/assets/imageHolder/logo_employee.jpg';
      this.selectedImage = null;
    }
  }

  onSubmit(formValue) {
    this.isSubmitted = true;
    console.log(formValue);
    if (this.formTemplate.valid) {
      const filePath = `${formValue.empDepartment}/${formValue.selectImage.name.split('.').slice(0, -1).join('.')}_${new Date().getTime()}`;
      const fileRef = this.storage.ref(filePath);
      this.storage.upload(filePath, formValue.selectImage).snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            formValue.imageUrl = url;
            console.log(formValue);
            this.service.insertEmployeeDetails(formValue);

          });
        })
      ).subscribe();
      this.resetForm();
      this.toastr.success('Adding Success!');
    } else {
      this.toastr.error('Adding Unsuccessful, please fill up everything!');
    }
  }

  get formControls() {
    return this.formTemplate.controls;
  }


  resetForm() {
    this.formTemplate.reset();
    this.formTemplate.setValue({
      empId: 'EMP_' + shortId.generate(),
      empFn: '',
      empMn: '',
      empLn: '',
      empBday: '',
      empPosition: '',
      empDepartment: '',
      imageUrl: '',
      selectImage: ''
    });
    this.imgSrc = '/assets/imageHolder/logo_employee.jpg';
    this.selectedImage = null;
    this.isSubmitted = false;
  }
}
