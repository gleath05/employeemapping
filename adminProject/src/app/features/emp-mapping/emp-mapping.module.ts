import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpMappingRoutingModule } from './emp-mapping-routing.module';
import { RightMappingComponent } from './components/right-mapping/right-mapping.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EmpMappingRoutingModule
  ]
})
export class EmpMappingModule { }
