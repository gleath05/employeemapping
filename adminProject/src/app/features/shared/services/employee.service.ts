import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  empDetailList: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase,
    private fireStorage: AngularFireStorage,
  )

  { 
    this.empDetailList = this.firebase.list('empDetails');
  }


  // user for pushing/adding employee
  insertEmployeeDetails(empDetails) {
    this.empDetailList.push(empDetails);
  }

  getEmployeeDetails() {
    this.empDetailList = this.firebase.list('empDetails');
    return this.empDetailList.snapshotChanges();
  }


}
